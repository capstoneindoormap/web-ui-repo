import 'dart:html';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

//Class that defines the home and the routes for navigating between pages
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
// Set default `_initialized` and `_error` state to false
  bool _initialized = false;
  bool _error = false;

  // Define an async function to initialize FlutterFire
  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Show error message if initialization failed
    if (_error) {}

    // Show a loader until FlutterFire is initialized
    if (!_initialized) {}

    return MaterialApp(home: MyHome(), routes: {
      '/List': (context) => MyList(),
      '/Details': (context) => MyDetails(),
      '/Analytics': (context) => Analytics()
    });
  }
}

//Login Page
class MyHome extends StatelessWidget {
  /*
  void initializeFlutterFire() async {
    try {
      await Firebase.initializeApp();
    } catch (e) {}
  }
  */

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Container(
          child: Column(
            children: [
              Text(
                'Vendor Login',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w900,
                    fontFamily: "Georgia",
                    color: Colors.black),
              ),
              TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Email",
                    fillColor: Colors.white),
              ),
              TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Password",
                    fillColor: Colors.white),
              ),
              TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/List");
                  },
                  child: Text("Login"),
                  style: TextButton.styleFrom(
                      primary: Colors.black, backgroundColor: Colors.blue))
            ],
          ),
          width: 460,
          height: 240,
          color: Colors.lightBlue,
        ),
      ),
    );
  }
}

//Page that lists stores
class MyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Container(
          child: ListView(
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.food_bank),
                title: TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/Details');
                  },
                  child: Text('Subway at Trafalgar Building D'),
                ),
              ),
              ListTile(
                leading: Icon(Icons.food_bank),
                title: TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/Details');
                  },
                  child: Text('Harveys at Trafalgar Building G'),
                ),
              ),
              ListTile(
                leading: Icon(Icons.food_bank),
                title: TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/Details');
                  },
                  child: Text('Thai Express at Trafalgar Food Court'),
                ),
              ),
            ],
          ),
          width: 600,
        ),
      ),
    );
  }
}

//Page that lists store details
class MyDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Container(
          child: Column(
            children: [
              Text(
                'Store Title',
                style: TextStyle(fontWeight: FontWeight.bold),
                textScaleFactor: 2.0,
              ),
              TextFormField(
                initialValue: 'Store Name',
                decoration: InputDecoration(
                    border: OutlineInputBorder(), fillColor: Colors.white),
              ),
              TextFormField(
                initialValue: 'Store Bio',
                minLines: 4,
                maxLines: 6,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), fillColor: Colors.white),
              ),
              SizedBox(height: 10),
              DropdownButton(
                value: 'Monday',
                items: <String>[
                  'Monday',
                  'Tuesday',
                  'Wednesday',
                  'Thursday',
                  'Friday',
                  'Saturday',
                  'Sunday'
                ].map((String value) {
                  return new DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
                onChanged: (_) {},
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Open: '),
                  Radio(value: 0, groupValue: 1, onChanged: null),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Closed: '),
                  Radio(value: 1, groupValue: 1, onChanged: null),
                ],
              ),
              SizedBox(height: 20),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Column(
                  children: [
                    Text('Opens at:'),
                    DropdownButton(
                      value: '7am',
                      items: <String>[
                        '7am',
                        '8am',
                        '9am',
                        '10am',
                        '11am',
                        '12pm',
                        '1pm'
                      ].map((String value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      onChanged: (_) {},
                    ),
                  ],
                ),
                SizedBox(width: 40),
                Column(
                  children: [
                    Text('Closes at:'),
                    DropdownButton(
                      value: '7pm',
                      items: <String>[
                        '7pm',
                        '8am',
                        '9am',
                        '10am',
                        '11am',
                        '12pm',
                        '1pm'
                      ].map((String value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      onChanged: (_) {},
                    ),
                  ],
                ),
              ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.black,
                        backgroundColor: Colors.grey,
                        padding: EdgeInsets.all(20),
                      ),
                      child: Text("Discard Changes"),
                      onPressed: () => Navigator.pop(context)),
                  TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.black,
                        backgroundColor: Colors.lightBlue,
                        padding: EdgeInsets.all(20),
                      ),
                      child: Text("Save Changes"),
                      onPressed: () => Navigator.pop(context)),
                ],
              ),
              SizedBox(height: 10),
              TextButton(
                  style: TextButton.styleFrom(
                    primary: Colors.black,
                    backgroundColor: Colors.lightGreen,
                    padding: EdgeInsets.fromLTRB(85, 20, 85, 20),
                  ),
                  child: Text("View Analytics"),
                  onPressed: () => Navigator.pushNamed(context, '/Analytics')),
            ],
          ),
          width: 600,
        ),
      ),
    );
  }
}

class Analytics extends StatefulWidget {
  @override
  _AnalyticsState createState() => _AnalyticsState();
}

class _AnalyticsState extends State<Analytics> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference analyticRef =
      FirebaseFirestore.instance.collection('Analytic');
  CollectionReference requestRef =
      FirebaseFirestore.instance.collection('Request');
  Future<void> addRequest() {
    return requestRef.add({'Status': "Started", 'userId': "xxdfD1558"});
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Container(
          child: Row(
            children: [
              SizedBox(width: 15),
              Column(
                children: [
                  TextButton(
                      onPressed: addRequest, child: Text("Request Analytic")),
                  FutureBuilder(
                    future: analyticRef.doc('Datter').get(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasError) {
                        return Text("Something has gone wrong");
                      }
                      if (snapshot.hasData && !snapshot.data!.exists) {
                        return Text(
                            "Analytic is not made/is still processing.\n Try again in a minute");
                      }
                      if (snapshot.connectionState == ConnectionState.done) {
                        Map<String, dynamic> data =
                            snapshot.data!.data() as Map<String, dynamic>;
                        return Text(
                            "Average time in line: ${data['timeInAvg']}s\n Longest wait time:${data['longy']}s\n Shortest wait time:${data['shorty']}s");
                      }
                      return Text('Loading');
                    },
                  ),
                  Row(),
                  Row(),
                ],
              ),
              SizedBox(width: 30),
              Column(
                children: [
                  Text('From:'),
                  DropdownButton(
                    value: '7am',
                    items: <String>[
                      '7am',
                      '8am',
                      '9am',
                      '10am',
                      '11am',
                      '12pm',
                      '1pm'
                    ].map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList(),
                    onChanged: (_) {},
                  ),
                  Text('To:'),
                  DropdownButton(
                    value: '7pm',
                    items: <String>[
                      '7pm',
                      '8am',
                      '9am',
                      '10am',
                      '11am',
                      '12pm',
                      '1pm'
                    ].map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList(),
                    onChanged: (_) {},
                  ),
                ],
              ),
              SizedBox(width: 30),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(children: [
                    Checkbox(value: true, onChanged: null),
                    Text('Monday')
                  ]),
                  Row(children: [
                    Checkbox(value: false, onChanged: null),
                    Text('Tuesday')
                  ]),
                  Row(children: [
                    Checkbox(value: false, onChanged: null),
                    Text('Wednesday')
                  ]),
                  Row(children: [
                    Checkbox(value: false, onChanged: null),
                    Text('Thursday')
                  ]),
                  Row(children: [
                    Checkbox(value: true, onChanged: null),
                    Text('Friday')
                  ]),
                  Row(children: [
                    Checkbox(value: true, onChanged: null),
                    Text('Saturday')
                  ]),
                  Row(children: [
                    Checkbox(value: true, onChanged: null),
                    Text('Sunday')
                  ]),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
